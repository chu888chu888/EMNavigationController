//
//  main.m
//  EMNavigationController
//
//  Created by EasonWang on 13-11-24.
//  Copyright (c) 2013年 EasonWang. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
